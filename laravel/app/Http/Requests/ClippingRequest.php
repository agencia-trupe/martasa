<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClippingRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'capa' => 'image',
            'link' => '',
            'video_tipo' => 'required_with:video_codigo',
            'video_codigo' => 'required_with:video_tipo',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
