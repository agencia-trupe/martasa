<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;

class ProjetosController extends Controller
{
    protected $categorias;

    public function __construct()
    {
        $this->categorias = ProjetoCategoria::ordenados()->get();
        
        view()->share('projetosCategorias', $this->categorias);    
    }

    public function index(ProjetoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = $this->categorias->first();
        }

        view()->share('categoriaSelecionada', $categoria);

        $projetos = $categoria ? $categoria->projetos : abort('404');

        return view('frontend.projetos.index', compact('projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        view()->share('categoriaSelecionada', $categoria);
        
        return view('frontend.projetos.show', compact('projeto'));
    }
}
