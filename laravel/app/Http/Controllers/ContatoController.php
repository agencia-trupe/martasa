<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect()->route('contato')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.contato', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('site')->nome_do_site)
               ->subject('[CONTATO] '.config('site')->nome_do_site)
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
