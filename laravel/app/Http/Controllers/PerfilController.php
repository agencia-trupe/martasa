<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Perfil;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Perfil::first();

        return view('frontend.perfil', compact('perfil'));
    }
}
