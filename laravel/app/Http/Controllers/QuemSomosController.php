<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\QuemSomos;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quemSomos = QuemSomos::first();

        return view('frontend.quem-somos', compact('quemSomos'));
    }
}
