<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $config = \App\Models\Configuracoes::first();

        config()->set('site', $config);
        config()->set('mail.from.name', $config->nome_do_site);

        view()->composer('*', function($view) use ($config) {
            $view->with('config', $config);
        });
        view()->composer('frontend.common.header', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
