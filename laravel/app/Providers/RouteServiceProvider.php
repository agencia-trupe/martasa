<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('quem-somos', 'App\Models\QuemSomos');
        $router->model('clipping', 'App\Models\Clipping');
		$router->model('imagens_clipping', 'App\Models\ClippingImagem');
		$router->model('projetos', 'App\Models\Projeto');
		$router->model('categorias_projetos', 'App\Models\ProjetoCategoria');
		$router->model('imagens_projetos', 'App\Models\ProjetoImagem');
		$router->model('perfil', 'App\Models\Perfil');
		$router->model('banners', 'App\Models\Banner');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categoria_slug', function($value) {
            return ProjetoCategoria::whereSlug($value)->firstOrFail();
        });
        $router->bind('projeto_slug', function($value) {
            return Projeto::whereSlug($value)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
