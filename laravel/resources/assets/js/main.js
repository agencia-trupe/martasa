import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.banner'
});

$('.fancybox').fancybox({
    padding: 0
});

$('.clipping-galeria').click(function(e) {
    e.preventDefault();

    var galeria = $(this).data('galeria');

    $('.fancybox[rel=galeria-' + galeria).first().trigger('click');
});

$('.clipping-video').fancybox({
    padding: 0,
    type: 'iframe',
    width: 1280,
    height: 720,
    aspectRatio: true
});
