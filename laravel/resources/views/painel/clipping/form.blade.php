@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa (opcional)') !!}
@if($submitText == 'Alterar' && $registro->capa)
    <img src="{{ url('assets/img/clipping/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    <a href="{{ route('painel.clipping.delete-capa', $registro->id) }}" class="btn-delete btn-delete-link label label-danger" style="display:inline-block;margin-bottom:10px">
        <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
        EXCLUIR
    </a>
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional, se preenchido precede a galeria de imagens)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="well" style="padding-bottom:5px">
    <div class="row">
        <div class="col-md-12" style="margin-bottom:10px">
            <label>Vídeo (opcional, se preenchido precede a galeria de imagens e link)</label>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('video_tipo', 'Tipo') !!}
                {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('video_codigo', 'Código') !!}
                {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clipping.index') }}" class="btn btn-default btn-voltar">Voltar</a>
