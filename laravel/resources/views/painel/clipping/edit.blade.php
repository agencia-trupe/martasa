@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clipping /</small> Editar Clipping</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.clipping.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.clipping.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
