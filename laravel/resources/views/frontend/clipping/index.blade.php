@extends('frontend.common.template')

@section('content')

    <div class="content clipping">
        <h1>CLIPPING</h1>

        <div class="clipping-thumbs">
            @foreach($clippings as $clipping)
                @include('frontend.clipping._thumb')
            @endforeach
        </div>

        <div style="display:none">
            @foreach($clippings as $clipping)
                @foreach($clipping->imagens as $imagem)
                <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $clipping->id }}"></a>
                @endforeach
            @endforeach
        </div>
    </div>

@endsection
