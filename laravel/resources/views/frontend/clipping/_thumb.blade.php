@if($clipping->video_codigo)
    <a href="{{ $clipping->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$clipping->video_codigo : 'https://player.vimeo.com/video/'.$clipping->video_codigo }}" class="clipping-video">
        @include('frontend.clipping._capa')
    </a>
@elseif($clipping->link)
    <a href="{{ $clipping->link }}" class="clipping-link" target="_blank">
        @include('frontend.clipping._capa')
    </a>
@else
    <a href="#" class="clipping-galeria" data-galeria="{{ $clipping->id }}">
        @include('frontend.clipping._capa')
    </a>
@endif
