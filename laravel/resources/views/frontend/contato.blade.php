@extends('frontend.common.template')

@section('content')

    <div class="content contato">
        <h1>CONTATO</h1>
        
        <div class="informacoes">
            <p class="telefones">{!! $contato->telefones !!}</p>
            <p class="endereco">{!! $contato->endereco !!}</p>
        </div>

        <form action="{{ route('contato.post') }}" method="POST">
            {{ csrf_field() }}

            @if(session('enviado'))
            <div class="enviado">Mensagem enviada com sucesso!</div>
            @endif
            @if($errors->any())
            <div class="erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            <div class="col">
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
            </div>
            <div class="col">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <input type="submit" value="ENVIAR">
            </div>
        </form>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>

        <div class="copyright">
            <p>
                © {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados
                <span>&middot;</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </div>

@endsection
