@extends('frontend.common.template')

@section('content')

    <div class="content perfil">
        <h1>PERFIL</h1>
        <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
        <div class="texto">
            {!! $perfil->texto !!}
        </div>
    </div>

@endsection
