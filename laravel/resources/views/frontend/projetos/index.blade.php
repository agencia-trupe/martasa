@extends('frontend.common.template')

@section('content')

    <div class="content projetos">
        <div class="categorias-mobile">
            @foreach($projetosCategorias as $categoria)
            <a href="{{ route('projetos', $categoria->slug) }}" @if($categoriaSelecionada->id == $categoria->id) class="active" @endif>{{ $categoria->titulo }}</a>
            @endforeach
        </div>

        <div class="projetos-index">
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos.show', [$projeto->categoria->slug, $projeto->slug]) }}">
                <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                <div class="overlay">
                    <span>{{ $projeto->titulo }}</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
