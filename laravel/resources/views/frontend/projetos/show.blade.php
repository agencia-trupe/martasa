@extends('frontend.common.template')

@section('content')

    <div class="content projetos">
        <div class="categorias-mobile">
            @foreach($projetosCategorias as $categoria)
            <a href="{{ route('projetos', $categoria->slug) }}" @if($categoriaSelecionada->id == $categoria->id) class="active" @endif>{{ $categoria->titulo }}</a>
            @endforeach
        </div>

        <div class="projeto-titulo">
            <span>{{ $projeto->titulo }}</span>
            <a href="{{ route('projetos', $projeto->categoria->slug) }}">VOLTAR</a>
        </div>

        <div class="projetos-show">
            @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria">
                <img src="{{ asset('assets/img/projetos/imagens/thumbs/'.$imagem->imagem) }}" alt="">
                <div class="overlay">
                    <span>AMPLIAR</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
