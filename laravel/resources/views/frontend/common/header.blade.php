    <header @if(Tools::isActive('home')) class="home" @endif>
        <a href="{{ route('home') }}" class="logo">
            {{ $config->nome_do_site }}
        </a>
        <nav>
            @include('frontend.common.nav')
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
