    <footer>
        <div class="center">
            <p>© {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.</p>
        </div>
    </footer>
