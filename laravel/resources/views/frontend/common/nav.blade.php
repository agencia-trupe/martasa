<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>home</a>
<a href="{{ route('quemsomos') }}" @if(Tools::isActive('quemsomos')) class="active" @endif>quem somos</a>
<a href="{{ route('perfil') }}" @if(Tools::isActive('perfil')) class="active" @endif>perfil</a>
<a href="{{ route('projetos') }}" @if(Tools::isActive('projetos*')) class="active" @endif>projetos</a>
<a href="{{ route('clipping') }}" @if(Tools::isActive('clipping')) class="active" @endif>clipping</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>contato</a>

@if($contato->facebook || $contato->instagram)
    <div class="social">
        @foreach(['facebook', 'instagram'] as $s)
            @if($contato->{$s})
            <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">
                {{ $s }}
            </a>
            @endif
        @endforeach
    </div>
@endif

@if(Tools::isActive('projetos*'))
    <div class="categorias">
        @foreach($projetosCategorias as $categoria)
        <a href="{{ route('projetos', $categoria->slug) }}" @if($categoriaSelecionada->id == $categoria->id) class="active" @endif>{{ $categoria->titulo }}</a>
        @endforeach
    </div>
@endif
