@extends('frontend.common.template')

@section('content')

    <div class="content quem-somos">
        <h1>QUEM SOMOS</h1>
        <div class="texto">
            {!! $quemSomos->texto !!}
        </div>
        <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem) }}" alt="">
    </div>

@endsection
